create database Schools
go
use Schools
go
create table Students
(
Id int primary key identity(1,1),
Name nvarchar(30) not null,
Age int default 18 not null,
Score int default 0
)
go
insert into Students(Name) values('李三')
insert into Students(Name) values('李四')
insert into Students(Name) values('张三')
insert into Students(Name) values('张四')
insert into Students(Name) values('刘三')
insert into Students(Name) values('刘四')
insert into Students(Name) values('王三')
insert into Students(Name) values('王四')
insert into Students(Name) values('小吴')